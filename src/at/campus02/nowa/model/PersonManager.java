package at.campus02.nowa.model;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class PersonManager {

	public static void main(String[] args) {

		Person person = new Person("Max", "Mustermann");
		
		writePerson(person);
		
		
		Person p1 = readPerson();
		
		System.out.println(p1);
	}

		public static void writePerson(Person p)
		{
			File file = new File("C:\\devdata\\PR2\\demo-person.txt");
					
			ObjectOutputStream oos = null; //ObjectOutputStream stellt Methoden zur Verf�gung, mit denen "gesamte Objekte" geschrieben werden k�nnen
			try
			{			
				oos	= new ObjectOutputStream(
						new BufferedOutputStream(     // Verf�gt �ber eine optimierten Zugriff (Puffer) auf Dateien
						new FileOutputStream(file))); //FileOutputStream schreibt Datei
				oos.writeObject(p);
				oos.flush();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			finally
			{
				if (oos != null)
				{
					try {
						oos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			}
		}
		
//		https://www.tutorialspoint.com/java/io/objectinputstream_readobject.htm{
		
		public static Person readPerson()
		{
			File file = new File("C:\\devdata\\PR2\\demo-person.txt");
			
			ObjectInputStream ois = null;
			try
			{
				ois = new ObjectInputStream(         //Stell Methoden zur Verf�gung, mit denen "gesamante Objekte" gelesen werden k�nnen
						new FileInputStream(file));  //List aus Dateien
				
				
				return (Person) ois.readObject();
				

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			finally
			{
				if (ois != null)
				{
					try {
						ois.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			}
			return null;
		}
	}
